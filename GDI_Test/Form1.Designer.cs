﻿namespace GDI_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxInterpolationMode = new System.Windows.Forms.ComboBox();
            this.labelInterpolationMode = new System.Windows.Forms.Label();
            this.comboBoxSmoothingMode = new System.Windows.Forms.ComboBox();
            this.labelSmoothingMode = new System.Windows.Forms.Label();
            this.labelCompositingQuality = new System.Windows.Forms.Label();
            this.comboBoxCompositingQuality = new System.Windows.Forms.ComboBox();
            this.labelPixelOffsetMode = new System.Windows.Forms.Label();
            this.comboBoxPixelOffsetMode = new System.Windows.Forms.ComboBox();
            this.labelTextRenderingHint = new System.Windows.Forms.Label();
            this.comboBoxTextRenderingHint = new System.Windows.Forms.ComboBox();
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(435, 326);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // comboBoxInterpolationMode
            // 
            this.comboBoxInterpolationMode.FormattingEnabled = true;
            this.comboBoxInterpolationMode.Location = new System.Drawing.Point(564, 12);
            this.comboBoxInterpolationMode.Name = "comboBoxInterpolationMode";
            this.comboBoxInterpolationMode.Size = new System.Drawing.Size(158, 21);
            this.comboBoxInterpolationMode.TabIndex = 2;
            this.comboBoxInterpolationMode.SelectedIndexChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // labelInterpolationMode
            // 
            this.labelInterpolationMode.Location = new System.Drawing.Point(456, 12);
            this.labelInterpolationMode.Name = "labelInterpolationMode";
            this.labelInterpolationMode.Size = new System.Drawing.Size(102, 21);
            this.labelInterpolationMode.TabIndex = 3;
            this.labelInterpolationMode.Text = "InterpolationMode:";
            this.labelInterpolationMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxSmoothingMode
            // 
            this.comboBoxSmoothingMode.FormattingEnabled = true;
            this.comboBoxSmoothingMode.Location = new System.Drawing.Point(564, 39);
            this.comboBoxSmoothingMode.Name = "comboBoxSmoothingMode";
            this.comboBoxSmoothingMode.Size = new System.Drawing.Size(158, 21);
            this.comboBoxSmoothingMode.TabIndex = 4;
            this.comboBoxSmoothingMode.SelectedIndexChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // labelSmoothingMode
            // 
            this.labelSmoothingMode.Location = new System.Drawing.Point(456, 39);
            this.labelSmoothingMode.Name = "labelSmoothingMode";
            this.labelSmoothingMode.Size = new System.Drawing.Size(102, 21);
            this.labelSmoothingMode.TabIndex = 5;
            this.labelSmoothingMode.Text = "SmoothingMode:";
            this.labelSmoothingMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelCompositingQuality
            // 
            this.labelCompositingQuality.Location = new System.Drawing.Point(456, 66);
            this.labelCompositingQuality.Name = "labelCompositingQuality";
            this.labelCompositingQuality.Size = new System.Drawing.Size(102, 21);
            this.labelCompositingQuality.TabIndex = 9;
            this.labelCompositingQuality.Text = "CompositingQuality:";
            this.labelCompositingQuality.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxCompositingQuality
            // 
            this.comboBoxCompositingQuality.FormattingEnabled = true;
            this.comboBoxCompositingQuality.Location = new System.Drawing.Point(564, 66);
            this.comboBoxCompositingQuality.Name = "comboBoxCompositingQuality";
            this.comboBoxCompositingQuality.Size = new System.Drawing.Size(158, 21);
            this.comboBoxCompositingQuality.TabIndex = 8;
            this.comboBoxCompositingQuality.SelectedIndexChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // labelPixelOffsetMode
            // 
            this.labelPixelOffsetMode.Location = new System.Drawing.Point(456, 93);
            this.labelPixelOffsetMode.Name = "labelPixelOffsetMode";
            this.labelPixelOffsetMode.Size = new System.Drawing.Size(102, 21);
            this.labelPixelOffsetMode.TabIndex = 11;
            this.labelPixelOffsetMode.Text = "PixelOffsetMode:";
            this.labelPixelOffsetMode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxPixelOffsetMode
            // 
            this.comboBoxPixelOffsetMode.FormattingEnabled = true;
            this.comboBoxPixelOffsetMode.Location = new System.Drawing.Point(564, 93);
            this.comboBoxPixelOffsetMode.Name = "comboBoxPixelOffsetMode";
            this.comboBoxPixelOffsetMode.Size = new System.Drawing.Size(158, 21);
            this.comboBoxPixelOffsetMode.TabIndex = 10;
            this.comboBoxPixelOffsetMode.SelectedIndexChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // labelTextRenderingHint
            // 
            this.labelTextRenderingHint.Location = new System.Drawing.Point(456, 120);
            this.labelTextRenderingHint.Name = "labelTextRenderingHint";
            this.labelTextRenderingHint.Size = new System.Drawing.Size(102, 21);
            this.labelTextRenderingHint.TabIndex = 13;
            this.labelTextRenderingHint.Text = "TextRenderingHint:";
            this.labelTextRenderingHint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxTextRenderingHint
            // 
            this.comboBoxTextRenderingHint.FormattingEnabled = true;
            this.comboBoxTextRenderingHint.Location = new System.Drawing.Point(564, 120);
            this.comboBoxTextRenderingHint.Name = "comboBoxTextRenderingHint";
            this.comboBoxTextRenderingHint.Size = new System.Drawing.Size(158, 21);
            this.comboBoxTextRenderingHint.TabIndex = 12;
            this.comboBoxTextRenderingHint.SelectedIndexChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // textBoxText
            // 
            this.textBoxText.Location = new System.Drawing.Point(564, 147);
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.Size = new System.Drawing.Size(156, 20);
            this.textBoxText.TabIndex = 14;
            this.textBoxText.Text = "♦╫╫SomeText╫╫♦";
            this.textBoxText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxText.TextChanged += new System.EventHandler(this.SomethingChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(453, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 21);
            this.label1.TabIndex = 15;
            this.label1.Text = "Example Text:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 350);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxText);
            this.Controls.Add(this.labelTextRenderingHint);
            this.Controls.Add(this.comboBoxTextRenderingHint);
            this.Controls.Add(this.labelPixelOffsetMode);
            this.Controls.Add(this.comboBoxPixelOffsetMode);
            this.Controls.Add(this.labelCompositingQuality);
            this.Controls.Add(this.comboBoxCompositingQuality);
            this.Controls.Add(this.labelSmoothingMode);
            this.Controls.Add(this.comboBoxSmoothingMode);
            this.Controls.Add(this.labelInterpolationMode);
            this.Controls.Add(this.comboBoxInterpolationMode);
            this.Controls.Add(this.pictureBox1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GDI_Test";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBoxInterpolationMode;
        private System.Windows.Forms.Label labelInterpolationMode;
        private System.Windows.Forms.ComboBox comboBoxSmoothingMode;
        private System.Windows.Forms.Label labelSmoothingMode;
        private System.Windows.Forms.Label labelCompositingQuality;
        private System.Windows.Forms.ComboBox comboBoxCompositingQuality;
        private System.Windows.Forms.Label labelPixelOffsetMode;
        private System.Windows.Forms.ComboBox comboBoxPixelOffsetMode;
        private System.Windows.Forms.Label labelTextRenderingHint;
        private System.Windows.Forms.ComboBox comboBoxTextRenderingHint;
        private System.Windows.Forms.TextBox textBoxText;
        private System.Windows.Forms.Label label1;
    }
}

