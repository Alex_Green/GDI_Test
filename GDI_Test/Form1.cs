﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Text;
using System.Windows.Forms;

namespace GDI_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            comboBoxInterpolationMode.DataSource = Array.FindAll((InterpolationMode[])Enum.GetValues(typeof(InterpolationMode)), (InterpolationMode IM) => { return IM != InterpolationMode.Invalid; });
            comboBoxSmoothingMode.DataSource = Array.FindAll((SmoothingMode[])Enum.GetValues(typeof(SmoothingMode)), (SmoothingMode SM) => { return SM != SmoothingMode.Invalid; });
            comboBoxCompositingQuality.DataSource = Array.FindAll((CompositingQuality[])Enum.GetValues(typeof(CompositingQuality)), (CompositingQuality CQ) => { return CQ != CompositingQuality.Invalid; });
            comboBoxPixelOffsetMode.DataSource = Array.FindAll((PixelOffsetMode[])Enum.GetValues(typeof(PixelOffsetMode)), (PixelOffsetMode POM) => { return POM != PixelOffsetMode.Invalid; });
            comboBoxTextRenderingHint.DataSource = Enum.GetValues(typeof(TextRenderingHint));
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();
        }

        private void SomethingChanged(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.InterpolationMode = (InterpolationMode)comboBoxInterpolationMode.SelectedValue;
            e.Graphics.SmoothingMode = (SmoothingMode)comboBoxSmoothingMode.SelectedValue;
            e.Graphics.CompositingQuality = (CompositingQuality)comboBoxCompositingQuality.SelectedValue;
            e.Graphics.PixelOffsetMode = (PixelOffsetMode)comboBoxPixelOffsetMode.SelectedValue;
            e.Graphics.TextRenderingHint = (TextRenderingHint)comboBoxTextRenderingHint.SelectedValue;

            RectangleF Re = pictureBox1.ClientRectangle;

            e.Graphics.DrawImage(Properties.Resources.gaechka, Re);

            e.Graphics.DrawRectangle(Pens.GreenYellow, Re.X, Re.Y, Re.Width, Re.Height);
            e.Graphics.DrawLine(Pens.GreenYellow, Re.Location, new PointF(Re.Width, Re.Height));
            e.Graphics.DrawLine(Pens.GreenYellow, new PointF(Re.X, Re.Bottom), new PointF(Re.Right, Re.Y));

            Font font = new Font("Times New Roman", 50f, FontStyle.Regular, GraphicsUnit.Pixel);
            StringFormat strFormat = new StringFormat();
            strFormat.Alignment = StringAlignment.Center;
            strFormat.LineAlignment = StringAlignment.Far;
            e.Graphics.DrawString(textBoxText.Text, font, Brushes.GreenYellow, Re, strFormat);

            ////////////////////////////////////////////////////////////
            strFormat.LineAlignment = StringAlignment.Near;

            Pen p = new Pen(Color.Black, 8); // for outline - set width parameter
            p.LineJoin = LineJoin.Round; //prevent "spikes" at the path

            RectangleF fontRe = new RectangleF(0, 0, Re.Width, font.Height);
            LinearGradientBrush b = new LinearGradientBrush(fontRe, Color.Blue, Color.Yellow, 90f);
            Blend blend = new Blend();
            blend.Factors = new float[] { 0f, 1f, 0f };
            blend.Positions = new float[] { 0f, 0.5f, 1f };
            b.Blend = blend;
            GraphicsPath gp = new GraphicsPath();
            gp.AddString(textBoxText.Text, font.FontFamily, (int)font.Style, font.Size, Re, strFormat);

            //TODO: shadow -> g.translate, fillpath once, remove translate
            e.Graphics.DrawPath(p, gp);
            e.Graphics.FillPath(b, gp);
            ////////////////////////////////////////////////////////////

            Re.Inflate(-Re.Width / 4f, -Re.Height / 4f);
            e.Graphics.DrawEllipse(Pens.GreenYellow, Re);
        }
    }
}
